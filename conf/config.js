/**
 * Example configurations for development and for production.
 * This config file will be changed while deploying, so use it
 * just like an example or doc.
 */
module.exports = {
    test: {
        redis_port: '6379',
        redis_host: 'localhost',
        port: 4005,
        // pusher socket.io settings
        pusher_origins : '*:*',
        pusher_heartbeats: true
    },
    unittest: {
        external_servers: {
            'hive-main-server': {
                port: 3003,
                ips: ['ip1', 'ip2', 'ip3']// pull of ips if you have more then one running server
            }
        },
        // pusher socket.io settings
        pusher_origins : '*:*',
        pusher_heartbeats: true
    }
};