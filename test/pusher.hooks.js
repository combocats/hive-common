process.env.NODE_ENV = 'test';
var exec     = require('child_process').exec;
var spawn    = require('child_process').spawn;
var request  = require('request');
var util     = require('util');
var config   = require('../conf/config')[process.env.NODE_ENV];
var _        = require('lodash');
var async    = require('async');
var redis    = require('redis');
var client   = redis.createClient(config.redis_port, config.redis_host);
var appProcess;
var testUsers = require('./test_users');

function line() { var line = ''; while(line.length < 50){ line+='*'} return line;}

console.log(line(), '\n* PREPARE TEST ENVIRONMENT');

function addUserSessionToRedis(callback) {
    var multi = client.multi();
    for(var id in testUsers) {
        var session = {
            cookie: {
                expires: new Date() + 1000*60*60*24,
                httpOnly: true,
                originalMaxAge: 86400000,
                path: "/"
            },
            passport: {
                user: id
            }
        };
        multi.set("sess:" + id, JSON.stringify(session));
    }
    multi.exec(function(err) {
        callback(err);
    });
}

function removeUserSessionFromRedis(callback) {
    var multi = client.multi();
    for(var id in testUsers) {
        multi.del("sess:" + id);
    }
    multi.exec(function(err) {
        callback(err);
    });
}

function killProcess() {
    if (appProcess) {
        console.log("* kill app process");
        console.log("* END UP TEST SESSION\n", line());
        appProcess.kill();
        appProcess = null;
    }
}

var before = function(done) {
    this.timeout(5000);

    appProcess = spawn('node', ['test/pusher.test.server.js'], {env: _.extend(process.env, { NODE_ENV: 'test' })});

    var noWhere = function(chunk) {
//        console.log(chunk.toString());
    };
    appProcess.stdout.on('data', noWhere);
    appProcess.stderr.on('data', noWhere);

    appProcess.on('exit', function (e) {
        console.log('\n\npusher test server terminated', e);
        console.log('\n\n');
    });

    /**
     * Kill the appProcess after test finished
     */
    process.on('exit', killProcess);

    /**
     * Waiting for server and drop db
     */
    function waitForServer(callback) {
        var url = util.format("http://localhost:%d/check", config.port);
        var interval = setInterval(function() {
            request(url, function(err, res, body) {
                console.log("* * checking server ...");
                if(res && res.statusCode == 200) {
                    console.log('* now server is running\n', line(), '\n\n\n');
                    clearInterval(interval);
                    callback();
                }
            });
        }, 500);
    }

    async.waterfall([
        function(cb) { addUserSessionToRedis(cb) },
        function(cb) { waitForServer(cb); }
    ], function(err) {
        setTimeout(function() {
            done(err);
        }, 200);
    });
};

var after = function(done) {
    async.waterfall([
        function(cb) { removeUserSessionFromRedis(cb) },
        function(cb) { killProcess(); cb(); }
    ], done);
};

module.exports = {
    before: before,
    after: after
};