var config = require('../conf/config')[process.env.NODE_ENV];
var _        = require('lodash');
var Pusher   = require('../lib/pusher');

var express  = require('express');

var app = express();

app.use(express.urlencoded());
app.use(express.json());
app.use(express.cookieParser());
app.use(express.methodOverride());

/* Config session storage */
var RedisStore = require('connect-redis')(express);
var redis = require('redis');
var sessionClient = redis.createClient(config.redis_port, config.redis_host);

var sessionConf = {
    secret: 'hive secret cat',
    store: new RedisStore({client: sessionClient})
};

app.use(express.session(sessionConf));

/**
 * Check api for availability tests.
 */
app.get('/check', function(req, res) {
    res.json({ok: true});
});

app.post('/send', function(req, res) {
    if(req.body.namespace) {
        Pusher.namespace(req.body.namespace).event(req.body.event).send(req.body.to, req.body.message);
    } else {
        Pusher.event(req.body.event).send(req.body.to, req.body.message);
    }
    res.json({ok: true});
});

var server = require('http').createServer(app);
server.listen(config.port);

Pusher.init(server, sessionConf, config.redis_port, config.redis_host);
Pusher.createNamespace('one');
Pusher.createNamespace('two');
console.log("listen on %s", config.port);
