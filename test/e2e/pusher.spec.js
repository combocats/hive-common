var hooks = require('../pusher.hooks');
var config = require('../../conf/config')['test'];
var io = require('socket.io-client');
var testUsers = require('../test_users');
var activeUser = testUsers[Object.keys(testUsers)[0]];
require('should');
var Modules = require('../modules');
var Pusher = require(Modules.pusher);

var request = require('request').defaults({
    headers: {
        'Content-Type': 'application/json'
    }
});

function url(url) {
    url = url ? url : '';
    return "http://localhost:" + config.port + url;
}

describe('lib.pusher', function() {

    before(hooks.before);
    after(hooks.after);

    describe('get /check', function() {

        it('should return game profile', function(done) {
            this.timeout(4000);
            request({
                url: url("/check"),
                method: "GET"
            }, function(err, res, body) {
                res.statusCode.should.be.equal(200);
                done();
            });
        });

    });

    describe('connect to socket server with error', function() {

        it('should emit handshake error for empty accessToken', function (done){
            var socket = io.connect(url(), {
                'force new connection': true
            });

            socket.on('error', function(error){
                error.should.be.equal('handshake unauthorized');
                done();
            });
        });

        it('should emit handshake error for not active accessToken', function (done){
            var socket = io.connect(url(), {
                'force new connection': true,
                query: 'accessToken=123'
            });
            socket.on('error', function(error){
                error.should.be.equal('handshake unauthorized');
                done();
            });
        });

        it('should emit error with unauthorized handshake', function (done){
            var socket = io.connect(url(), {
                'force new connection':true,
                query: 'accessToken=' + activeUser._id
            });
            socket.on('connect', function(){
                socket.disconnect();
                done();
            });
        });

    });

    describe('connect to socket server', function() {

        var socket;
        before(function(done) {
            socket = io.connect(url(), {
                'force new connection' : true,
                query : 'accessToken=' + activeUser._id
            });

            socket.on('connect', function(res) {
                done();
            });
        });

        it('should catch server send event for specified namespace', function (done) {
            socket.of('/one').on('new', function(res) {
                res.should.eql({ ok: true });
                done();
            });

            request({
                method: 'POST',
                url: url("/send"),
                json: {
                    namespace: 'one',
                    to: activeUser._id,
                    event: 'new',
                    message: {
                        ok: true
                    }
                }
            });
        });

        it('should catch server send event for another namespace', function (done) {
            socket.of('/two').on('new', function(res) {
                res.should.eql({ ok: false });
                done();
            });

            request({
                method: 'POST',
                url: url("/send"),
                json: {
                    namespace: 'two',
                    to: activeUser._id,
                    event: 'new',
                    message: {
                        ok: false
                    }
                }
            });
        });

        it('should catch server send event without specifying namespace', function (done) {
            socket.on('new', function(res) {
                res.should.eql({ all: true});
                done();
            });

            request({
                method: 'POST',
                url: url("/send"),
                json: {
                    to: activeUser._id,
                    event: 'new',
                    message: {
                        all: true
                    }
                }
            });
        });

    });

});

