process.env.NODE_ENV = 'unittest';
var should  = require('should');
var Q       = require('q');
var _       = require('lodash');
var proxyquire =  require('proxyquire');

var Modules = require('../modules');
var errors  = require(Modules.errors)();

describe('lib.external_servers', function() {

    describe('validateConfig', function() {

        var ES;
        before(function() {
            ES = require(Modules.externalServers);
        });

        it('should be ok for valid config', function(done) {
            var config = {
                port: 3000,
                ips: ['192.168.1.1']
            };

            ES.validateConfig(config).then(function(data) {
                data.should.be.ok;
            }, function() {
                'error'.should.be.equal('success');
            }).done(done);
        });

        it('should throw error for empty config', function(done) {
            var config = {};

            ES.validateConfig(config).then(function() {
                'success'.should.be.equal('error');
            }, function(err) {
                err.code.should.be.equal(errors.WRONG_EXTERNAL_SERVER_CONFIG().code);
                err.should.have.property('err');
            }).done(done);
        });

        it('should throw error for config without port property', function(done) {
            var config = {
                ips: []
            };

            ES.validateConfig(config).then(function() {
                'success'.should.be.equal('error');
            }, function(err) {
                err.code.should.be.equal(errors.WRONG_EXTERNAL_SERVER_CONFIG().code);
                err.should.have.property('err');
            }).done(done);
        });

    });

    describe('request', function() {

        var ES;
        var request;

        before(function() {
            var attemptsCount = 0;
            request = function() {
                if(attemptsCount == request.attempt) {
                    return _.last(arguments)(null, {statusCode: 200}, {ok: true});
                } else {
                    attemptsCount++;
                    return _.last(arguments)({err: 'error'});
                }
            };
            request.attempt = 0;
            request.defaults = function() {};

            ES = proxyquire(Modules.externalServers, {
                './settings': {
                    config: {
                        external_servers: {
                            'test-server': {
                                port: 3000,
                                ips: ['ip1', 'ip2', 'ip3']
                            }
                        }
                    }
                },
                'request': request
            });
            ES.attemptsTimeout = 1;
        });

        it('should try to make request again if first one faild', function(done) {
            request.attempt = 2;
            ES.use('test-server').request('GET', '/url').then(function(data) {
                data.should.be.ok;
            }, function(err) {
                'error'.should.be.equal('success');
            }).done(done);
        });

        it('should fail after several attempts to request the server', function(done) {
            request.attempt = 5;
            ES.use('test-server').request('GET', '/url').then(function() {
                'success'.should.be.equal('error');
            }, function(err) {
                err.code.should.be.equal(errors.COULD_NOT_CALL_EXTERNAL_SERVER().code);
                err.should.have.property('attempts');
                err.attempts.should.be.equal(3);
            }).done(done);
        });

    });

    describe('check', function() {

        var ES;
        var request;

        before(function() {
            request = function() {
                return _.last(arguments)(request.response.err, request.response.res, request.response.body);
            };
            request.response = {};
            request.defaults = function() {};

            ES = proxyquire(Modules.externalServers, {
                './settings': {
                    config: {
                        external_servers: {
                            'test-server': {
                                port: 3000,
                                ips: ['ip1', 'ip2']
                            }
                        }
                    }
                },
                'request': request
            });
        });

        it('should return /check api response', function(done) {
            request.response = {
                err: null,
                res: {statusCode: 200},
                body: {ok: true}
            };

            ES.check('test-server').then(function(data) {
                data.should.be.ok;
            }, function(err) {
                'error'.should.be.equal('success');
            }).done(done);
        });

        it('should return error if external server respond with non 200 status', function(done) {
            request.response = {
                err: null,
                res: {statusCode: 400},
                body: {status: 400, code: 100000}
            };

            ES.check('test-server').then(function() {
                'success'.should.be.equal('error');
            }, function(err) {
                err.code.should.be.equal(errors.EXTERNAL_SERVER_RESPOND_WITH_ERROR().code);
            }).done(done);
        });

    });

});
