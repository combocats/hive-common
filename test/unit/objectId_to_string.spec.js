process.env.NODE_ENV = 'unittest';
var should  = require('should');

var Modules = require('../modules');
var errors  = require(Modules.errors)();

describe('lib.objectId_to_string', function() {

    var ObjectID;
    var objectIDToString;
    before(function() {
        objectIDToString = require(Modules.objectIDToString);

        ObjectID = function ObjectID(id) {
            this.id = id;
            return this;
        };

        ObjectID.prototype.toString = function() {
            return this.id;
        };
    });

    it('should replace ObjectId with string in root', function() {
        var data = {
            _id: new ObjectID('12345')
        };

        data._id.should.be.type('object');
        objectIDToString(data);
        data._id.should.be.type('string');
    });

    it('should replace two properties of ObjectId with string in root', function() {
        var data = {
            _id: new ObjectID('12345'),
            post: new ObjectID('12346')
        };

        data._id.should.be.type('object');
        data.post.should.be.type('object');
        objectIDToString(data);
        data._id.should.be.type('string');
        data.post.should.be.type('string');
    });

    it('should replace ObjectId of EmbeddedDocument with _doc property', function() {
        var data = {
            user: {
                _doc : {
                    _id: new ObjectID('12345')
                },
                some: 'some',
                func: function() {}
            }
        };

        data.user.should.be.type('object');
        objectIDToString(data);
        data.user.should.be.type('object');
        data.user._id.should.be.type('string');
        data.user.should.eql({
            _id: '12345'
        });
    });

    it('should replace ObjectId with string in second level property', function() {
        var data = {
            user: {
                _id: new ObjectID('12345')
            }
        };

        data.user._id.should.be.type('object');
        objectIDToString(data);
        data.user._id.should.be.type('string');
    });

    it('should replace ObjectId with string in second and third level properties', function() {
        var data = {
            user: {
                _id: new ObjectID('12345'),
                post: {
                    _id: new ObjectID('12345')
                }
            }
        };

        data.user._id.should.be.type('object');
        data.user.post._id.should.be.type('object');
        objectIDToString(data);
        data.user._id.should.be.type('string');
        data.user.post._id.should.be.type('string');
    });

    it('should replace Array of ObjectIds with strings', function() {
        var data = {
            posts: [new ObjectID('12345'), new ObjectID('12345')]
        };

        data.posts[0].should.be.type('object');
        objectIDToString(data);
        data.posts[0].should.be.type('string');
        data.posts[1].should.be.type('string');
    });



    it('should not change simple object', function() {
        var data = {
            user: {
                name: 'string',
                age : 12
            }
        };

        data.user.should.be.type('object');
        objectIDToString(data);
        data.user.should.be.type('object');
        data.user.name.should.be.type('string');
        data.user.age.should.be.type('number');
    });

});