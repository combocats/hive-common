process.env.NODE_ENV = 'unittest';
var should  = require('should');

var Modules = require('../modules');
var errors  = require(Modules.errors)();

describe('lib.validate', function() {

    var V = require(Modules.validate);

    describe('pickOnly', function() {

        it('should fetch only specified properties', function(done) {
            var req = {
                body: {
                    email: 'zahar@lodossteam.com',
                    password: 'qwerty123',
                    wrong: 123
                }
            };
            V.pickOnly(req, 'body', ['email', 'password']).then(function() {
                req.body.should.have.keys([
                    'email',
                    'password'
                ]);
            }, function() {
                'error'.should.be.equal('success');
            }).done(done);
        });

    });

    describe('mustHaveKeys', function() {

        it('should be ok if all properties are defined', function(done) {
            var req = {
                body: {
                    email: 'zahar@lodossteam.com',
                    password: 'qwerty123'
                }
            };
            V.mustHaveKeys(req.body, ['email', 'password']).then(function() {
                'success'.should.be.ok;
            }, function() {
                'error'.should.be.equal('success');
            }).done(done);
        });

        it('should throw error if not all properties are defined', function(done) {
            var req = {
                body: {
                    email: 'zahar@lodossteam.com'
                }
            };
            V.mustHaveKeys(req.body, ['email', 'password']).then(function() {
                'success'.should.be.equal('error');
            }, function(error) {
                error.code.should.be.equal(errors.MISSING_REQUIRED_FIELDS().code);
            }).done(done);
        });

        it('should throw error if empty object', function(done) {
            var req = {
                body: {}
            };
            V.mustHaveKeys(req.body, ['email', 'password']).then(function() {
                'success'.should.be.equal('error');
            }, function(error) {
                error.code.should.be.equal(errors.MISSING_REQUIRED_FIELDS().code);
            }).done(done);
        });

    });

    describe('removeKeys', function() {

        it('should remove specified keys from object', function(done) {
            var req = {
                body: {
                    _id: '123',
                    friend: 'qwerty2134',
                    email: 'zahar@lodossteam.com',
                    password: 'qwerty123'
                }
            };
            V.removeKeys(req.body, ['friend', '_id']).then(function() {
                'success'.should.be.ok;
                req.body.should.not.have.properties([
                    '_id',
                    'friend'
                ]);
            }, function() {
                'error'.should.be.equal('success');
            }).done(done);
        });

    });

    describe('checkTypes', function() {

        it('should be ok if types are correct', function(done) {
            var req = {
                body: {
                    string: 'zahar@lodossteam.com',
                    number: 1234,
                    object: {},
                    array: [],
                    boolean: true
                }
            };
            V.checkTypes(req.body, {
                'string': String,
                'number': Number,
                'object': Object,
                'array': Array,
                'boolean': Boolean
            }).then(function() {
                'success'.should.be.ok;
            }, function(error) {
                'error'.should.be.equal('success');
            }).done(done);
        });

        it('should throw error if types are not correct', function(done) {
            var req = {
                body: {
                    string: 123,
                    number: 'string',
                    object: 123,
                    array: 123,
                    boolean: 123
                }
            };
            V.checkTypes(req.body, {
                'string': String,
                'number': Number,
                'object': Object,
                'array': Array,
                'boolean': Boolean
            }).then(function() {
                'success'.should.be.equal('error');
            }, function(error) {
                error.code.should.be.equal(errors.NOT_CORRECT_TYPE().code);
                error.keys.should.include('string');
                error.keys.should.include('number');
                error.keys.should.include('object');
                error.keys.should.include('array');
                error.keys.should.include('boolean');
            }).done(done);
        });

        it('should check type if specified key is not defined', function(done) {
            var req = {
                body: {
                    string: 'test'
                }
            };
            V.checkTypes(req.body, {
                'string': String,
                'number': Number
            }).then(function() {
                true.should.be.ok;
            }, function(error) {
                'error'.should.be.equal('success');
            }).done(done);
        });

    });

    describe('email', function() {

        it('should be ok for valid email', function(done) {
            var req = {
                email: 'z.andreev@lodossteam.com'
            };
            V.email(req, 'email').then(function() {
                'success'.should.be.ok;
            }, function() {
                'error'.should.be.equal('success');
            }).done(done);
        });

        it('should throw error for not valid email', function(done) {
            var req = {
                email: 'z.@.andreev@lodossteam.c'
            };
            V.email(req, 'email').then(function() {
                'success'.should.be.equal('error');
            }, function(error) {
                error.should.be.ok;
                error.code.should.be.equal(errors.NOT_VALID_EMAIL().code);
            }).done(done);
        });

    });

    describe('password', function() {

        it('should be ok for strong password', function(done) {
            var password = 'qwerty123';
            V.password(password).then(function() {
                'success'.should.be.ok;
            }, function() {
                'error'.should.be.equal('success');
            }).done(done);
        });

        it('should be ok for password that starts from digits', function(done) {
            var password = '123qwerty';
            V.password(password).then(function() {
                'success'.should.be.ok;
            }, function(error) {
                'error'.should.be.equal('success');
            }).done(done);
        });

        it('should return error for not latin chars', function(done) {
            var password = 'йцукенг123';
            V.password(password).then(function() {
                'success'.should.be.equal('error');
            }, function(error) {
                error.should.be.ok;
            }).done(done);
        });

        it('should return error if password is weak', function(done) {
            var password = 'qwerty';
            V.password(password).then(function() {
                'success'.should.be.equal('error');
            }, function(error) {
                error.code.should.be.equal(errors.WEAK_PASSWORD().code);
            }).done(done);
        });

        it('should return error if password is not defiend', function(done) {
            var password;
            V.password(password).then(function() {
                'success'.should.be.equal('error');
            }, function(error) {
                error.code.should.be.equal(errors.WEAK_PASSWORD().code);
            }).done(done);
        });

    });

    describe('shouldBeInArray', function() {

        it('should be ok for the value in specified array', function(done) {
            var items = ['one', 'two', 'three'];
            var req = {
                body: {
                    item: 'one'
                }
            };
            V.shouldBeInArray(req.body, 'item', items).then(function() {
                'success'.should.be.ok;
            }, function() {
                'error'.should.be.equal('success');
            }).done(done);
        });

        it('should be ok for undefined value', function(done) {
            var items = [];
            var req = {
                body: {}
            };
            V.shouldBeInArray(req.body, 'item', items).then(function() {
                'success'.should.be.ok;
            }, function() {
                'error'.should.be.equal('success');
            }).done(done);
        });

        it('should return error is value is not in array', function(done) {
            var items = ['one', 'two', 'three'];
            var req = {
                body: {
                    item: 'five'
                }
            };
            V.shouldBeInArray(req.body, 'item', items).then(function() {
                'success'.should.be.equal('error');
            }, function(error) {
                error.code.should.be.equal(errors.VALUE_IS_NOT_IN_SET().code);
                error.key.should.be.equal('item');
                error.value.should.be.equal('five');
            }).done(done);
        });

    });

    describe('shouldBeAProperty', function() {

        it('should be ok for the value in specified object', function(done) {
            var items = {
                one: {},
                two: {},
                three: {}
            };
            var req = {
                body: {
                    item: 'one'
                }
            };
            V.shouldBeAProperty(req.body, 'item', items).then(function() {
                'success'.should.be.ok;
            }, function() {
                'error'.should.be.equal('success');
            }).done(done);
        });

        it('should be ok for undefined value', function(done) {
            var items = {};
            var req = {
                body: {}
            };
            V.shouldBeAProperty(req.body, 'item', items).then(function() {
                'success'.should.be.ok;
            }, function() {
                'error'.should.be.equal('success');
            }).done(done);
        });

        it('should return error for the value not in specified object', function(done) {
            var items = {
                one: {},
                two: {},
                three: {}
            };
            var req = {
                body: {
                    item: 'five'
                }
            };
            V.shouldBeAProperty(req.body, 'item', items).then(function() {
                'success'.should.be.equal('error');
            }, function(error) {
                error.code.should.be.equal(errors.VALUE_IS_NOT_IN_SET().code);
                error.key.should.be.equal('item');
                error.value.should.be.equal('five');
            }).done(done);
        });

    });

    describe('arrayValuesShouldBeProperties', function() {

        it('should be ok for the values in specified object', function(done) {
            var items = {
                one: {},
                two: {},
                three: {},
                four: {}
            };
            var req = {
                body: {
                    items: ['one', 'two']
                }
            };
            V.arrayValuesShouldBeProperties(req.body, 'items', items).then(function() {
                'success'.should.be.ok;
            }, function(error) {
                'error'.should.be.equal('success');
            }).done(done);
        });

        it('should be ok for undefined array', function(done) {
            var items = {};
            var req = {
                body: {}
            };
            V.arrayValuesShouldBeProperties(req.body, 'items', items).then(function() {
                'success'.should.be.ok;
            }, function(error) {
                'error'.should.be.equal('success');
            }).done(done);
        });

        it('should return error for a single value not in specified object', function(done) {
            var items = {
                one: {},
                two: {},
                three: {}
            };
            var req = {
                body: {
                    items: ['one', 'five']
                }
            };
            V.arrayValuesShouldBeProperties(req.body, 'items', items).then(function() {
                'success'.should.be.equal('error');
            }, function(error) {
                error.code.should.be.equal(errors.ARRAY_ELEMENT_IS_NOT_IN_SET().code);
                error.key.should.be.equal('items');
                error.value.should.be.equal('five');
            }).done(done);
        });

    });

    describe('shouldHaveLength', function() {

        it('should be ok for the values in specified object', function(done) {
            var req = {
                body: {
                    items: ['one', 'two']
                }
            };
            V.shouldHaveLength(req.body, 'items', 2).then(function() {
                'success'.should.be.ok;
            }, function(error) {
                'error'.should.be.equal('success');
            }).done(done);
        });

        it('should be ok for undefined array', function(done) {
            var req = {
                body: {}
            };
            V.shouldHaveLength(req.body, 'items', 2).then(function() {
                'success'.should.be.ok;
            }, function(error) {
                'error'.should.be.equal('success');
            }).done(done);
        });

        it('should return error for array of wrong length', function(done) {
            var req = {
                body: {
                    items: ['one', 'five']
                }
            };
            V.shouldHaveLength(req.body, 'items', 5).then(function() {
                'success'.should.be.equal('error');
            }, function(error) {
                error.code.should.be.equal(errors.NOT_VALID_ARRAY_LENGTH().code);
                error.key.should.be.equal('items');
            }).done(done);
        });

    });

    describe('chain', function() {

        it('should start validation chain', function(done) {
            var req = {
                body: {
                    email: 'test@test.com',
                    string: 'test',
                    number: 123
                }
            };
            V.chain([
                V.mustHaveKeys(req.body, ['email', 'string']),
                V.checkTypes(req.body, {
                    'string': String
                }),
                V.checkTypes(req.body, {
                    'number': Number
                })
            ], function(error) {
                (!error).should.be.ok;
                done();
            });
        });

        it('should chain should return array of errors', function(done) {
            var req = {
                body: {
                    string: 123,
                    number: 123
                }
            };
            V.chain([
                V.mustHaveKeys(req.body, ['email']),
                V.checkTypes(req.body, {
                    'string': String
                }),
                V.checkTypes(req.body, {
                    'number': Number
                })
            ], function(error) {
                error.should.be.ok;
                error.should.have.property('code');
                done();
            });
        });

    });

    describe('shouldBeDefined', function() {

        it('should be ok for the value in specified object', function(done) {
            var data = {
                user: true
            };
            V.shouldBeDefined(data, ['user']).then(function() {
                'success'.should.be.ok;
            }, function() {
                'error'.should.be.equal('success');
            }).done(done);
        });

        it('should be rejected for not existing property', function(done) {
            var data = {};
            V.shouldBeDefined(data, ['user']).then(function() {
                'success'.should.be.equal('error');
            }, function(error) {
                error.code.should.be.equal(errors.REQUIRED_PROPERTY_IS_UNDEFINED().code);
            }).done(done);
        });

        it('should be rejected for not defined property', function(done) {
            var data = {
                user: undefined
            };
            V.shouldBeDefined(data, ['user']).then(function() {
                'success'.should.be.equal('error');
            }, function(error) {
                error.code.should.be.equal(errors.REQUIRED_PROPERTY_IS_UNDEFINED().code);
            }).done(done);
        });

    });

});