var path = require('path');
var _ = require('lodash');

var Modules = {
    'settings'          : './lib/settings',
    'errors'            : './lib/errors',
    'validate'          : './lib/validate',
    'externalServers'   : './lib/external_servers',
    'objectIDToString'  : './lib/objectId_to_string',
    'pusher'            : './lib/pusher'
};

var absolutPaths = {};
_.forEach(Modules, function(value, key) {
    absolutPaths[key] = path.resolve(value);
});

module.exports = absolutPaths;