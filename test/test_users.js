module.exports = {
    '529c19753255720000000001': {
        _id: '529c19753255720000000001',
        name: 'Zahar',
        uid: 'ABC100',
        timezone: -240,
        email: 'zahar@lodossteam.com',
        friends: ['529c19753255720000000002']
    },
    '529c19753255720000000002': {
        _id: '529c19753255720000000002',
        name: 'Max',
        uid: 'ABC101',
        timezone: -120,
        email: 'max@lodossteam.com',
        friends: ['529c19753255720000000001']
    }
};