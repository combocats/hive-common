exports.settings = require('./lib/settings');
exports.errors   = require('./lib/errors');
exports.validate = require('./lib/validate');
exports.logger   = require('./lib/logger').getLogger;

exports.cors        = require('./lib/cors');
exports.tokenAccess = require('./lib/token_access');
exports.endPoints   = require('./lib/api_endpoint');

exports.inheritObject = require('./lib/inherit_object');

exports.pusher        = require('./lib/pusher');

exports.externalServers   = require('./lib/external_servers');
exports.objectIDToString  = require('./lib/objectId_to_string');

try {
    require('mongoose');
    exports.modifiedTimestamp = require('./lib/mongoose_timestamp');
} catch (error) {
    console.log("no mongoose package, so 'modifiedTimestamp' module did not included");
}