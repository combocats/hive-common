var _ = require('lodash');
var moment = require('moment');
var log = require('./logger').getLogger('errors');

/**
 * Build set of error functions from specified error objects.
 * @returns {Object} - with error functions
 */
module.exports = function() {
    /*
     * Merge all error modules in single place and add common validation and common errors.
     */
    [].push.call(arguments, require('./errors_validate'));
    [].push.call(arguments, require('./errors_common'));
    var errors = _.extend.apply(_, arguments);

    /*
     * Check errors for possible duplicates of 'code' property.
     * Will throw exception with array of found duplicates.
     */
    var errorsArray = _.toArray(errors);
    var duplicates = _.difference(errorsArray, _.uniq(errorsArray, 'code'));
    if(duplicates.length) {
        var message = JSON.stringify({
            info: "Look 'duplicates' Array for list of all found duplicates",
            duplicates: duplicates
        });

        throw {
            name: "Duplicated error codes are found in './lib/errors' module",
            message: message
        };
    }

    /*
     * Make functions with ability to extend default error message
     * (add some new fields or rewrite existed).
     */
    var errorFunctions = {};
    _.forEach(errors, function(value, key) {
        errorFunctions[key] = function(data) {
            log.error(key);
            data = data ? data : {};
            data.timestamp = moment(new Date()).format('YYYY-MM-DD HH:MM:ss.S');
            return _.extend(value, data);
        };
    });

    errorFunctions.makeHTMLDoc = function(output) {
        var fs = require('fs');
        var docTemplate = fs.readFileSync(__dirname + '/errors.tpl.html', 'utf8');
        fs.writeFile(output, _.template(docTemplate, {errors: errors}),  function(err) {
            console.log("write errors doc file error: ", err);
        });
    };

    return errorFunctions;
};