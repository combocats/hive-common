var _ = require("lodash");
var util = require("util");

module.exports = function(source) {
    var target = function() {
        source.apply(this, arguments);
        return this;
    };

    util.inherits(target, source);
    for(var prop in source) {
        target[prop] = _.cloneDeep(source[prop]);
    }

    return target;
};