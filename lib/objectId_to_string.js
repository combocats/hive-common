function format(data) {
    if(data) {
        if('ObjectID' === data.constructor.name) {
            data = String(data);
        } else if ('Object' === data.constructor.name) {
            var keys = Object.keys(data);
            var i = keys.length;
            var key;

            while(i--) {
                key = keys[i];
                if(data[key]) {
                    if('ObjectID' === data[key].constructor.name) {
                        data[key] = String(data[key]);
                    } else if (Array.isArray(data[key])) {
                        data[key] = data[key].map(function(o) {
                            /**
                             * We need to work with our model, so for Arrays of EmbeddedDocuments
                             * we must use ._doc property (actual model and not Mongoose EmbeddedDocument object)
                             */
                            return format(o._doc || o);
                        });
                    } else if('Object' === data[key].constructor.name) {
                        data[key] = format(data[key]._doc || data[key]);
                    }
                }
            }
        }
    }
    return data;
}

module.exports = function(data) {
    return format(data);
};