var _ = require('lodash');
var log = require('./logger').getLogger('app');
var errors = require('./errors')();

/**
 * End point for all error requests to public API
 */
exports.error = function(err, req, res, next) {
    log.error("api error in route [%s], query ", req.path, req.query, err);
    if(!err.code || !err.status) {
        err = errors.UNEXPECTED_ERROR({error: err});
    }
    res.status(err.status);
    res.json(err);
};


/**
 * End point for all non error requests to api
 */
exports.success = function(req, res) {
    log.debug('end api call by sending response data');
    if(_.isUndefined(res.data)) {
        res.status(404);
        return res.json();
    }
    if(_.isNull(res.data)) {
        return res.json();
    }
    res.json(res.data);
};

/**
 * Checker for modified resource (array or object).
 */
exports.modified = function(req, res, next) {
    log.debug('check that resource has changed or not', req.param('modified'));
    var checkDate = (new Date(req.param('modified'))).valueOf();
    if(checkDate) {
        var modified;
        if(_.isArray(res.data)) {
            modified = new Date(_.max(res.data, 'modified').modified).valueOf();
        } else {
            modified = new Date(res.data.modified).valueOf();
        }
        if(checkDate == modified) {
            log.info("requested resource not modified");
            res.status(304);
            res.data = null;
        }
    }

    next();
};

exports.addVersion = function(version) {
    return function(req, res, next) {
        log.debug('end api call to add version data to response');
        
        if(!_.isUndefined(res.data) && !_.isNull(res.data) && version) {
            res.data.conf_current_version = version;
        }

        next();
    };
};