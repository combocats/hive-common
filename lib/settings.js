var env  = process.env.NODE_ENV || 'dev';
var path = require('path');

var Settings = {
    env     : env,
    config  : require(path.resolve(process.cwd(), 'conf/config'))[env]
};

module.exports = Settings;
