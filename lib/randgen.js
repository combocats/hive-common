module.exports = {
    /**
     * Generate uniformly distributed random numbers
     * Gives a random number on the interval [min, max).
     * If discrete is true, the number will be an integer.
     * @param min
     * @param max
     * @param discrete
     * @returns {*} Uniform random number.
     */
    runif: function runif(min, max, discrete) {
        if (min === undefined) {
            min = 0;
        }
        if (max === undefined) {
            max = 1;
        }
        if (discrete === undefined) {
            discrete = false;
        }
        if (discrete) {
            return Math.floor(runif(min, max, false));
        }
        return Math.random() * (max - min) + min;
    }
};