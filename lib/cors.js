/**
 * Express middleware for CORS.
 * @param req
 * @param res
 * @param next
 */
module.exports = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    res.header("Keep-Alive", "timeout=10");

    next();
};