var log     = require('./logger').getLogger('pusher');
var _       = require('lodash');
var redis   = require('redis');
var express = require('express');
var config  = require('./settings').config;

var Pusher = function() {};
Pusher.io = null;

function authenticate(data, accept) {
    log.debug("auth data", data);

    if(data.query && data.query.accessToken) {
        data.sessionID = data.query.accessToken;
    } else {
        log.error("accessToken not found in the query");
        return accept(null, false);
    }
    log.debug("session id: ", data.sessionID);
    Pusher.store.get("sess:" + data.sessionID, function(err, session) {
        log.debug("get info from session", err, session);

        if(err) {
            log.error("error on getting session from store", err);
            return accept(null, false);
        }

        if(!session) {
            log.warn("no session found");
            return accept(null, false);
        }

        try {
            session = JSON.parse(session);
        } catch (err) {
            return accept(null, false);
        }

        if(session.passport && session.passport.user) {
            log.info("user found and authorized", session.passport.user);
            data.user = session.passport.user;
            return accept(null, true);
        }

        return accept(null, false);
    });
}

/**
 * Initialize pusher with specified params.
 * @param {object} server - NodeJS http server
 * @param {object} session - Express session object.
 * @param {string} redisPort - Redis port for pub/sub channels.
 * @param {string} redisHost - Redis host for pub/sub channels.
 */
Pusher.init = function(server, session, redisPort, redisHost) {
    log.info("initialize socket io module");
    Pusher.io = require('socket.io').listen(server, {
        'destroy upgrade': false
    });
    var SocketRedisStore = require('socket.io/lib/stores/redis');
    var pub = redis.createClient(redisPort, redisHost);
    var sub = redis.createClient(redisPort, redisHost);
    var ioClient = redis.createClient(redisPort, redisHost);

    Pusher.store = redis.createClient(redisPort, redisHost);

    Pusher.io.configure(function() {
        Pusher.io.set( 'origins', config.pusher_origins );
        Pusher.io.set( 'heartbeats', config.pusher_heartbeats );
        Pusher.io.set('log level', 2);
        Pusher.io.set('store', new SocketRedisStore({
            redisPub : pub,
            redisSub : sub,
            redisClient : ioClient
        }));

        Pusher.io.set('transports', [
            'websocket'
//            'xhr-polling',
//            'htmlfile',
//            'jsonp-polling'
        ]);

        // join passport and socket.io authorizations
        Pusher.io.set('authorization', authenticate);
    });

    Pusher.io.sockets.on('connection', function(socket) {
        log.info("socket connected", socket.handshake.user, socket.id);
        // connect user id with sockets rooms
        socket.join("room_" + socket.handshake.user);
    });

    Pusher.io.sockets.on('disconnect', function(socket) {
        log.debug("socket disconnected for user", socket.handshake.user);
        socket.leave("room_" + socket.handshake.user);
    });
};

/**
 * Create new namespace in Pusher for sending events in a part of application.
 * @param {string} namespace - The name of created namespace.
 */
Pusher.createNamespace = function(namespace) {
    namespace = '/' + namespace;

    Pusher.io.of(namespace).on('connection', function(socket) {
        log.info("socket namespace connected", namespace, socket.handshake.user, socket.id);
        // connect user id with sockets rooms
        socket.join("room_" + socket.handshake.user);
        log.debug("after joining a room");
    });

    Pusher.io.of(namespace).on('disconnect', function(socket) {
        log.debug("socket namespace disconnected for user", namespace, socket.handshake.user);
        socket.leave("room_" + socket.handshake.user);
    });
};

/**
 * Send specified socket event to specified user. Should be used with methods:
 *
 *  + .namespace(name) - send message in specified namespace
 *  + .event(event) - the name of the emitted event
 *
 *  If you use it like Pusher.event().send() event will be emitted to all connected sockets.
 *
 * @param {string} to - The id of the user.
 * @param {object} message - Data to send.
 */
Pusher.send = function(to, message) {
    if(_.isUndefined(Pusher.prototype.event)) {
        throw { message: 'Use Pusher.event(eventName) before using .send() method' };
    }

    log.debug("send notification to user", Pusher.prototype.namespace, Pusher.prototype.event, message);
    if(Pusher.prototype.namespace) {
        Pusher.io.of('/' + Pusher.prototype.namespace).in("room_" + to).emit(Pusher.prototype.event, message);
    } else {
        Pusher.io.sockets.in("room_" + to).emit(Pusher.prototype.event, message);
    }

    Pusher.prototype.namespace = null;
    Pusher.prototype.event = null;
};

/**
 * Setup game before using .send method.
 * @param {string} name - The name of the namespace.
 * @returns {object} Returns Pusher.
 */
Pusher.namespace = function(name) {
    log.debug("set namespace", name);
    Pusher.prototype.namespace = name;
    return Pusher;
};

/**
 * Specify the name of event that will be emitted by calling .send() method.
 * @param {string} name - The name of the event.
 * @returns {object} Returns Pusher.
 */
Pusher.event = function(name) {
    log.debug("set event name", name);
    Pusher.prototype.event = name;
    return Pusher;
};

module.exports = Pusher;
