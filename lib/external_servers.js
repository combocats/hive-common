var _ = require('lodash');
var Q = require('q');
var V = require('./validate');

var log = require('./logger').getLogger('external_servers');
var errors = require('./errors')();
var runif = require('./randgen').runif;

var headers = {
    'Content-Type': 'application/json'
};
var request = require('request');
request.defaults({
    headers: headers
});

var settings = require('./settings');

if(settings.config)
    var external_servers = settings.config.external_servers ? settings.config.external_servers : {};
else
    external_servers = {};

var ExternalServers = {};

ExternalServers.serverName = null;

/**
 * Timeout between attempts to request external server (ms).
 * @default 500ms
 * @type {number}
 */
ExternalServers.attemptsTimeout = 500;

/**
 * Maximum number of attempts to request external server.
 * @default 3
 * @type {number}
 */
ExternalServers.maxAttempts = 3;

/**
 * Check that external sserver config is valid.
 * @param config
 * @returns {Promise}
 */
ExternalServers.validateConfig = function(config) {
    return Q.nbind(V.chain, V)([
        V.mustHaveKeys(config, ['port', 'ips']),
        V.checkTypes(config, {port: Number, ips: Array})
    ]).then(function() {
            return {ok: true};
        }, function(err) {
            return Q.reject(errors.WRONG_EXTERNAL_SERVER_CONFIG({err: err}))
        });
};

/**
 * Check connection to specified external server. If config is ok and server
 * is available promise will be resolved with /rpc/check response.
 * @param {string} serverName - Server name.
 * @returns {Promise} Resolved with /rpc/check response or rejected with error.
 */
ExternalServers.check = function(serverName) {
    if(!external_servers[serverName]) return Q.reject(errors.OBJECT_IS_UNDEFINED({key: serverName}));

    return ExternalServers.validateConfig(external_servers[serverName])
        .then(function() {
            return ExternalServers.use(serverName).get('/rpc/check')
                .then(function(data) {
                    log.info("external server check success", serverName);
                    return data;
                }, function(err) {
                    log.error("external server check failed", serverName, err);
                    return Q.reject(err);
                });
        });
};

/**
 * Switch module to using specified external server as end point.
 * @param {string} serverName - Name of the server.
 * @returns {object} ExternalServers object with http shortcuts.
 */
ExternalServers.use = function(serverName) {
    ExternalServers.serverName = serverName;
    return ExternalServers;
};

/**
 * Makes absolute url from relative. Adds random ip address if there more then one
 * in ips array of config.
 * @param {string} url - Relative url.
 * @returns {string} Absolute url.
 */
ExternalServers.url = function(url) {
    var ips = external_servers[ExternalServers.serverName].ips;
    var port = external_servers[ExternalServers.serverName].port;
    var base = 'http://' + ips[runif(0, ips.length, 1)] + ':' + port;

    return base + url;
};


/**
 * Make GET request to specified with .use() external server. All non 200
 * responses will be interpreted as error.
 * @param {string} url - Relative URL address.
 * @param {object} data - Request body.
 * @returns {Promise} Promise resolved with server response or error.
 */
ExternalServers.get = function(url, data) {
    return ExternalServers.request('GET', url, data);
};

/**
 * Make POST request to specified with .use() external server. All non 200
 * responses will be interpreted as error.
 * @param {string} url - Relative URL address.
 * @param {object} data - Request body.
 * @returns {Promise} Promise resolved with server response or error.
 */
ExternalServers.post = function(url, data) {
    return ExternalServers.request('POST', url, data);
};

/**
 * Universal method for all request types. It will make several attempts to request
 * the server with different ips if there is more then one in config.
 * @param {string} method - HTTP method [get, post, put, delete]
 * @param {string} url - Relative url.
 * @param {object} data - Request body.
 * @returns {promise|*|Q.promise}
 */
ExternalServers.request = function(method, url, data) {
    var attemptsCount = 1;
    var defer = Q.defer();

    function makeRequest() {
        var URL = ExternalServers.url(url);
        request({
            url: URL,
            method: method,
            json: data || {}
        }, function(err, res, body) {
            log.info("request made", {attempt: attemptsCount, url: URL, data: data});
            if(err) {
                log.warn("could not call external server", err, attemptsCount);
                if(attemptsCount < ExternalServers.maxAttempts) {
                    attemptsCount++;
                    return setTimeout(makeRequest, ExternalServers.attemptsTimeout);
                } else {
                    return defer.reject(errors.COULD_NOT_CALL_EXTERNAL_SERVER({err: err, attempts: attemptsCount}));
                }
            }

            if(res.statusCode != 200) {
                log.error("external server respond with error", body);
                return defer.reject(body)
            }

            return defer.resolve(body);
        });
    }

    makeRequest();

    return defer.promise;
};

module.exports = ExternalServers;