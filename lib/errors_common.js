module.exports = {
    USER_NOT_AUTHENTICATED: {
        status: 403,
        code: 100001,
        message: 'Forbidden for not authorized users'
    },
    UNEXPECTED_ERROR: {
        status: 500,
        code: 199999,
        message: 'Unexpected error happened'
    },
    WRONG_EXTERNAL_SERVER_CONFIG: {
        status: 500,
        code: 100002,
        message: 'Wrong config for external server'
    },
    COULD_NOT_CALL_EXTERNAL_SERVER: {
        status: 500,
        code: 100003,
        message: 'Could not call external server'
    },
    EXTERNAL_SERVER_RESPOND_WITH_ERROR: {
        status: 500,
        code: 100004,
        message: 'External server respond with error'
    },
    WRONG_ACCESS_ROLE: {
        status: 403,
        code: 100005,
        message: 'User have no access rights'
    },
    NOT_ACTUAL_RESOURCE: {
        status: 409,
        code: 100006,
        message: 'You version is outdate. Please, refresh and try again'
    }
};