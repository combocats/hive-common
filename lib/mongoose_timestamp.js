var mongoose = require('mongoose');
var log = require('./logger').getLogger('modified_timestamp');

var timestampKey = 'modified';

mongoose.Model.__findOneAndUpdate = mongoose.Model.findOneAndUpdate;

mongoose.Model.findOneAndUpdate = function(query, update, options, callback) {
    update[timestampKey] = Date.now();
    log.debug("override findOneAndUpdate", query, update);
    return mongoose.Model.__findOneAndUpdate.call(this, query, update, options, callback);
};

mongoose.Model.__update = mongoose.Model.update;

mongoose.Model.update = function(conditions, doc, options, callback) {
    doc[timestampKey] = Date.now();
    log.debug("override update", conditions, doc);
    return mongoose.Model.__update.call(this, conditions, doc, options, callback);
};