var _ = require('lodash');
var Q = require('q');
var log = require('./logger').getLogger('validate');
var errors = require('./errors')();
var should = require('should');
var maybe  = require('maybe2');

var Validate = {};

/**
 * Accepts array of validation promises, and call next() with error if any
 * @param validators
 * @param next
 */
Validate.chain = function(validators, next) {
    Q.all(validators)
        .done(function() {
            return next();
        }, function(error) {
            log.warn('validation error', error);
            return next(error);
        });
};

/**
 * Get only restricted properties from req body
 * @param obj
 * @param property
 * @param keys
 * @returns {exports|*}
 */
Validate.pickOnly = function(obj, property, keys) {
    obj[property] = _.pick(obj[property], keys);
    return Q(obj);
};

/**
 * Checks that all apecified keys exist and defined.
 * @param obj
 * @param keys
 * @returns {}
 */
Validate.shouldBeDefined = function(obj, keys) {
    for(var key in keys) {
        if(_.isUndefined(obj[keys[key]])) {
            return Q.reject(errors.REQUIRED_PROPERTY_IS_UNDEFINED());
        }
    }
    return Q(true);
};

/**
 * Remove tags from specified keys of object
 * @param obj
 * @returns {exports|*}
 */
Validate.sanitize = function(obj, keys) {
    var regex = /(<([^>]+)>)/ig;
    _.forEach(keys, function(key) {
        if(obj[key])
            obj[key] = obj[key].replace ? obj[key].replace(regex, "") : obj[key];
    });
    return Q(obj);
};

/**
 * Must have all specified keys without any exception.
 * @param obj
 * @param keys
 * @returns {*}
 */
Validate.mustHaveKeys = function(obj, keys) {
    try {
        obj.should.have.properties(keys);
        return Q(true);
    } catch (err) {
        return Q.reject(errors.MISSING_REQUIRED_FIELDS({required: keys}));
    }
};

/**
 * Remove not allowed properties.
 * @param obj
 * @param keys
 * @returns {*}
 */
Validate.removeKeys = function(obj, keys) {
    var i = keys.length;
    do {
        delete obj[keys[i]];
        i--;
    } while(i >= 0)
    return Q(true);
};

/**
 * Check specified keys for correct type.
 * @param obj
 * @param map {object} - {keyToCheck: correctType}, i.e. {email: String, password: String}
 * @returns {exports|*}
 */
Validate.checkTypes = function(obj, map) {
    var errorKeys = [];
    for(var key in map) {
        if(obj[key] && !maybe(obj[key]).is(map[key]).getOrElse(null)) {
            errorKeys.push(key);
        }
    }
    return errorKeys.length ? Q.reject(errors.NOT_CORRECT_TYPE({keys: errorKeys})) : Q(true);
};

/**
 * Check that specified value with key of obj is one of vset values.
 * @param obj
 * @param key
 * @param vset
 * @returns {exports|*}
 */
Validate.shouldBeInArray = function(obj, key, vset) {
    if(obj[key]) {
        return _.contains(vset, obj[key]) ? Q(true) : Q.reject(errors.VALUE_IS_NOT_IN_SET({key: key, value: obj[key]}));
    } else {
        return Q(true);
    }
};

/**
 * Check that specified value is one of the properties of vset.
 * @param obj
 * @param key
 * @param vset
 * @returns {exports|*}
 */
Validate.shouldBeAProperty = function(obj, key, vset) {
    if(obj[key]) {
        return vset[obj[key]] ? Q(true) : Q.reject(errors.VALUE_IS_NOT_IN_SET({key: key, value: obj[key]}));
    } else {
        return Q(true);
    }
};

/**
 * Check that all the values in array are allowed.
 * @param obj
 * @param key
 * @param vset
 * @returns {*}
 */
Validate.arrayValuesShouldBeProperties = function(obj, key, vset) {
    if(maybe(obj[key]).is(Array).getOrElse(null)) {
        for(var i in obj[key]) {
            if(!vset[obj[key][i]]) {
                return Q.reject(errors.ARRAY_ELEMENT_IS_NOT_IN_SET({key: key, value: obj[key][i]}));
            }
        }
        return Q(true);
    } else {
        return Q(true);
    }
};

/**
 * Check that apecified array or string has correct length.
 * @param obj
 * @param key
 * @param length
 * @returns {exports|*}
 */
Validate.shouldHaveLength = function(obj, key, length) {
    if(obj[key] && obj[key].length != undefined) {
        return obj[key].length == length ? Q(true) : Q.reject(errors.NOT_VALID_ARRAY_LENGTH({key: key, length: obj[key].length}));
    } else {
        return Q(true);
    }
};

/**
 * Validate password: should have more then 6 chars and more then 1 digit among them.
 * @param password
 * @returns {exports|*}
 */
Validate.password = function(password) {
    if(typeof password != 'string') return Q.reject(errors.WEAK_PASSWORD());
    if(!(/\D/.test(password) && /\d/.test(password))) return Q.reject(errors.WEAK_PASSWORD());
    if(password.length <= 6) return Q.reject(errors.WEAK_PASSWORD());

    if(password.length > 25) return Q.reject(errors.TOO_LONG_PASSWORD());

    if(!/^[A-Za-z0-9]+$/.test(password)) return Q.reject(errors.NOT_ALLOWED_CHARS_IN_PASSWORD());

    return Q(password);
};

/**
 * Check that email is valid.
 * @param obj
 * @param key
 * @returns {*}
 */
Validate.email = function(obj, key) {
    if(!obj || !obj[key]) return Q(true);

    var result = maybe(obj[key])
        .is(String)
        .map(function(str) {
            return /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(str) ? str : null;
        })
        .getOrElse(null);

    return result ? Q(result) : Q.reject(errors.NOT_VALID_EMAIL());
};

module.exports = Validate;