/**
 * Token based authorization for CORS and non-cookie agents
 * @param req
 * @param res
 * @param next
 */
module.exports = function(req, res, next) {
    if(req.param('accessToken')) {
        req.cookies['connect.sid'] = req.param('accessToken');
    }
    next();
};